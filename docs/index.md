The purpose of the ISA SIG is to quantify the potential benefits of
applying existing compiler technology to distribution packages,
targeting more recent CPUs, and evaluating different options for how
these optimizations can be maintained in a scalable way, and delivered
to end users.

Our initial focus is the x86_64 architecture, but other architectures may be included in the future.

Today, there is a significant delay between the release of new x86-64
ISAs and the adoption across the entire CentOS distribution. For
example, CentOS 9 Stream released with the x86-64-v2 ISA baseline, nine
to thirteen years after such CPUs became commercially available. The
concern is that this discrepancy leaves more recent CPU features unused,
and end users do not see the best possible performance for the CPUs they
use.

Parts of the distribution are built with run-time selected
optimizations, but this only applies to specialized functional areas,
such as string manipulation, certain mathematical operations, and
cryptography, but not (for example) to language interpreters that are
part of CentOS.

We welcome anybody that's interested and willing to do work within the scope of the SIG to join and contribute.

This documentation is still a work in progress and we'd welcome any [contributions](contributing/documentation.md).
