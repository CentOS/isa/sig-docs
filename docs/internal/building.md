# Working with git

The ISA SIG uses [Gitlab](https://gitlab.com/CentOS/isa) for our project work.  At the moment, we have two main areas: [rpms](https://gitlab.com/CentOS/isa/rpms) and [sig-docs](https://gitlab.com/CentOS/isa/sig-docs).

## The `rpms` group

The rpms group contains a set of packages that have been mirrored from CentOS Stream which need to be adjusted/rebased/modified on top of whatever is currently included.  The projects are setup to pull mirror from the corresponding CentOS Stream repos.  For SIG specific changes, we have two main branches:

* `c9s-sopt`: Changes that correspond to optimized versions of the CentOS Stream packages.  Currently this work is focused on an x86_64-v3 baseline rebase.
* `c9s-sbase`: Changes that are needed on top of the existing CentOS Stream packages for baseline comparison purposes.

## The sig-docs project

At the moment the `sig-docs` project is a catch-all project for the rest of the SIG work.  It contains the sources for the SIG documentation, and acts as a project to file SIG issues against.

Documentation is in markdown format, using the mkdoc tooling to generate the output.

# Code Workflow

## Creating a package project

If you're working on optimizing a specific package and the project for the package is not present in the `rpms` group, file an issue against the sig-docs project requesting that it be added.  These should just be packages present in CentOS Stream already, but if there is a net-new package to add then include information on where it can be found.  The ISA SIG aims to focus on optimizing packages found in CentOS Stream and Fedora first and foremost.

## Cloning a package

To clone a package
```
$git clone git@gitlab.com:CentOS/isa/rpms/gcc.git
```

Sources may be in the CentOS Stream or SIG lookaside cache, depending on if the package has deviated or not.

## Making changes

When making changes to a package that exists in CentOS Stream already, use the proper branch for the kind of work as descrbied above.  E.g.

```
git checkout -b c9s-sopt
```

We strongly encourage the Merge Request workflow, so often it is better to fork the project into your own namespace and work on a local branch that you will then submit to the SIG package repo as a Merge Request.  These will be merged and you can then do builds.

Changes that need new sources uploaded to the lookaside can upload them with:

```
lookaside_upload_sig -f <tarball> -n <package name>
```

and then include the update SOURCES file in the MR.

# Build targets

We currently have two main build targets

* `isa9s-packages-baseline-el9sbase` -- the baseline build target, which is used to provide a GCC 12 based x86_64-v2 baseline build of CentOS Stream
* `isa9s-packages-optimized-el9sopt` -- the optimization build target, which is used to provide a GCC 12 based x86_64-v3 build of CentOS Stream

# Destination tags

For each build target there are three destination tags:

* `${tag}-candidate` - regular builds end up here by default and will be included in the buildroot for subsequent builds
* `${tag}-testing` - builds tagged here end up on buildlogs.centos.org
* `${tag}-release` - builds tagged here end up on mirrors.centos.org

# Build order

The `annobin`/`gcc` bootstrap is a bit tricky.  The following rebuild
order works.  These steps must have wait-repo operations between them.

* `redhat-rpm-config` build.  This introduces some CBS workarounds, and
  sets `-march=x86-64-v3` for the x86-64-v3 rebuilds.  (The second part
  does not apply to the baseline builds).
* First `annobin` build.  This disabled the hard version requirement
  on a GCC major release, which is a relic from older times where
  there was no automated plugin switch.
* `gcc` rebuild.
* Second `annobin` build.  After this, the plugin switch uses the the
  annobin plugin under the annobin name once more, which is what some
  packages expect when they disable the plugin.  The spec file has NVR
  switch magic, so that the same sources can be used to produce a
  different NVR after the GCC rebase.

After that, the following packages should be built in the indicated
order, with wait-repo operations between (if there are multiple such
packages, that is).

* `libtool`

The next batch involves the language interpreters.  These could be
built in parallel (although Python depends on Tcl).

* `bash`
* `lua`
* `make`
* `perl`
* `python3.9`
* `ruby`
* `tcl`

After another wait-repo, the following groups can be built in parallel
(but serialized within the group).

* `llvm`, wait-repo, `clang` (both packages are build dependencies
  for other things in the distribution)
* `selinux-policy` (required because other policy packages include
  a versioned dependency, and the dist tag change in the rebuild may
  result in a lower version-release).
* `rpm` (core package, it makes sense to rebuild it early)

After that, the resut of the distribution can be built in parallel.

# Build failures

Some packages fail to build in mainline `c9s` as well.  Build failure
logs for these packages are available in this Git repository:

* https://gitlab.com/CentOS/isa/c9s-logs