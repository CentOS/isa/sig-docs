# How to join

* Send an email to the centos-devel mailing list with ideas/interest in the ISA SIG
* Join #centos-isa on Libera.Chat
* File an [issue](https://gitlab.com/CentOS/isa/sig-docs/-/issues) requesting to join the ISA SIG.  State what you want to work on and why it's important to you.

# Onboarding

New member side:

* Before joining the SIG:
  * Follow the [Authentication](https://sigs.centos.org/guide/auth/) section of the SIG Guide.
  * We use Gitlab for our repos.  Read the [Lookaside/Git](https://sigs.centos.org/guide/git/) section of the SIG Guide.
  * Install [mock-centos-sig-configs](https://src.fedoraproject.org/rpms/mock-centos-sig-configs)
  * Set up the [packager tools](https://docs.fedoraproject.org/en-US/package-maintainers/Installing_Packager_Tools/)
    * Set up your `~/.fedora.upn` to tell our tooling your FAS username if it differs from your Unix name
    * Make sure to acquire a Fedora Kerberos ticket as well. More information and debugging tips can be found [here](https://fedoraproject.org/wiki/Infrastructure/Kerberos).
  * Install [centos-packager](https://wiki.centos.org/Authentication#TLS_certificate)
  * Review the SIG git, buildroot, and process documentation

SIG side:

* Add the CentOS account to https://accounts.centos.org/group/sig-isa
* Update the members list on the members page
