This [documentation site](https://sigs.centos.org/isa/) is generated
from our [Gitlab repository](https://gitlab.com/CentOS/isa/sig-docs.git) using
[mkdocs](https://www.mkdocs.org). It uses the same
[configuration](https://git.centos.org/centos/centos-infra-docs/blob/master/f/mkdocs.yml)
as the CentOS Infra documentation (notably, the
[material theme](https://squidfunk.github.io/mkdocs-material/)).

To test changes, run `make test` which will spawn a webserver on localhost. You
can also run `make build` to render the documentation as a static site.
