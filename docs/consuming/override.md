# Override Repo #

The ISA sign is maintaining an Override repo of packages.

These are packages that are named the same, or conflict with
packages in RHEL, but have been compiled with some type of
optimized flags or features.

These packages are expected to be maintained and have
security, feature and/or compile-time options updated.

## Setup ##

### CentOS Stream 9 ###

The extras-common is enabled by default.  So you should just need to run

```
dnf install centos-release-isa-override
```

### RHEL or RHEL Compatible ###

```
dnf install https://mirror.stream.centos.org/SIGs/9-stream/extras/x86_64/extras-common/Packages/c/centos-release-isa-override-9-2.el9s.noarch.rpm
```

## List Available Packages ##

After you have setup the repo.  You can list what is available.

```
dnf --repo=centos-isa-override list available
```
