**Convert CentOS Stream 9 to ISA**

These instructions assume that you have CentOS Stream 9 installed.

If you have problems with these instructions, please let us know so we can update them, or make them easier to understand.


**ISA Baseline**

ISA Baseline packages were compiled with gcc-12.  Regular CentOS Stream 9 was compiled with gcc-11.

* Install the centos-release-isa-baseline rpm
```
dnf -y install https://buildlogs.centos.org/9-stream/isa/x86_64/packages-baseline/Packages/c/centos-release-isa-baseline-1-1.el9sbase_902.noarch.rpm
```
* Convert over to ISA
```
dnf distro-sync -y --allowerasing
reboot
```


**ISA Optimized**

ISA Baseline packages were compiled with gcc-12, but they also had x86_64-v3 enabled.  Regular CentOS Stream 9 was compiled with gcc-11 and x86_64-v2.

* Install the centos-release-isa-optimized rpm
```
dnf -y install https://buildlogs.centos.org/9-stream/isa/x86_64/packages-optimized/Packages/c/centos-release-isa-optimized-1-1.el9sopt_902.noarch.rpm
```
* Convert over to ISA
```
dnf distro-sync -y --allowerasing
reboot
```

**distro-sync problems**

If you are having problems with the distro-sync of your CentOS Stream 9 installation, make sure you have
`--allowerasing` set

If that does not fix your problem, install using a CentOS Stream 9 DVD that we have verified will distro-sync,
using the instructions above.

* [CentOS Stream 9 Installation DVD - 2023-10-23](https://buildlogs.centos.org/centos/9-stream/BaseOS/x86_64/iso/CentOS-Stream-9-20231023.1-x86_64-dvd1.iso)
* [other CentOS Stream 9 Images - 2023-10-23](https://buildlogs.centos.org/centos/9-stream/BaseOS/x86_64/images/)
